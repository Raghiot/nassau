import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import logoHome from '../utils/nassau-logo-ok.png'
import { motion } from "framer-motion"
import HomeLogo from '@/components/HomeLogo'
import localFont from '@next/font/local'

const inter = Inter({ subsets: ['latin'] })
const myFont = localFont({ src: '../utils/font/Monthoers.otf' })

export default function Home() {
  console.log(styles.main)
  return (
    <>
      <Head>
        <title>Nassau</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      
      <main className={`${styles.main + " " + styles.navbarwood + " " + myFont.className }`}>
        
        <HomeLogo className={`${styles.main}`}></HomeLogo>
        



      </main>
    </>
  )
}
