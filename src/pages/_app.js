import '@/styles/globals.css'
// add bootstrap css
import 'bootstrap/dist/css/bootstrap.css'
// import Font Awesome CSS
import '@fortawesome/fontawesome-svg-core/styles.css'

import { config } from '@fortawesome/fontawesome-svg-core'
// Tell Font Awesome to skip adding the CSS automatically
// since it's already imported above
config.autoAddCss = false
// own css files here
// import "../css/customcss.css";

export default function App ({ Component, pageProps }) {
  return <Component {...pageProps} />
}
