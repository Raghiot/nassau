import { useEffect, useState } from 'react'
import styles from '@/styles/Home.module.css'
import localFont from '@next/font/local'
import homePageStyle from './HomePage.module.css'
import SocialBar from '@/components/SocialBar'


const myFont = localFont({ src: '../../utils/font/Monthoers.otf' })
export default function HomePage () {
  const [windowWidth, setWindowWidth] = useState()
  useEffect(() => {
    setWindowWidth(window.innerWidth)
    console.log(windowWidth)
  }, [windowWidth])
  return (
    <div
      className={`${
        styles.homePageBackGroud + ' ' + myFont.className
      } container-fluid vh-100`}
    >
      <div className='row justify-content-center pt-5'>
        <div className='col-2'></div>
        <div className='col-4 mt-5 pt-5'>
          <div className='row ms-sm-5'>
            <div className='col-12'>
              <h1
                className={`${myFont.className} text-white w-100 ${homePageStyle.title} ps-1`}
              >
                nassau
              </h1>
              <SocialBar></SocialBar>
            </div>
          </div>
        </div>
        <div className='col-6'></div>
      </div>
    </div>
  )
}
