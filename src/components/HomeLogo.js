import logoHome from '../utils/nassau-logo-ok.png'
import { motion } from 'framer-motion'
import Image from 'next/image'
import styles from '@/styles/Home.module.css'
import { useEffect, useState } from 'react'
import Link from 'next/link'


export default function HomeLogo () {
  const [windowWidth, setWindowWidth] = useState()
  useEffect(() => {
    setWindowWidth(window.innerWidth)
    console.log(windowWidth)
  }, [windowWidth])
  return (
    <div className='container-fluid'>
      <div className='row justify-content-center pt-5'>
        <div
          className={
            styles.typewriter + ' w-auto col-12 d-none d-sm-block mt-5'
          }
        >
          <h1 className='text-uppercase'>
            Cantiere delle forme e dei linguaggi
          </h1>
        </div>
        <div className='col-12 h-100 justify-content-center d-flex pb-0 mb-0 pe-0 ps-2'>
          <motion.div
            initial={{ opacity: 0, scale: 0.5 }}
            animate={{ opacity: 1, scale: 1 }}
            transition={{
              delay: 1,
              x: { duration: 1 },
              default: { ease: 'linear' }
            }}
          >
            <Image
              src={logoHome}
              className={`${windowWidth > 780 ? '' : 'img-fluid'}`}
              alt='...'
              width={`${windowWidth > 780 ? 550 : null}`}
            />
          </motion.div>
        </div>
        <div className={styles.typewriter + ' w-auto col-12'}>
          <Link
            href='/homePage/HomePage'
            className='link-dark text-decoration-none'
            replace
          >
            <h2 className='text-uppercase'>Discover us!</h2>
          </Link>
        </div>
      </div>
    </div>
  )
}
