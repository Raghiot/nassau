import Image from "next/image";
import logo from "../utils/pittogramma-nassau-circolare.gif"
import styles from '@/styles/Home.module.css'

export default function NavBar() {
    
    return (
        <nav className={`${"bg-transparent navbar position-sticky top-0 border-dark border-bottom d-sm-none d-block"}`}>
            <div className="container">
                <a className="navbar-brand" href="#">
                    <Image src={logo}  alt="..." className="img-fluid" width={50}/>
                </a>
            </div>
        </nav>
    )
}