import FbIcon from '../utils/icons/Fb'
import IgIcon from '../utils/icons/Ig'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function SocialBar () {
  return (
    <div className='container-fluid mt-3'>
      <div className='row p-0'>
        <div className='col-12 p-0 '>
          <ul className='list-group list-group-horizontal p-0'>
            <li className='list-group-item bg-transparent p-0 text-white me-2'>
              <a href='https://www.instagram.com/nassaubologna/'>
                <IgIcon></IgIcon>
              </a>
            </li>
            
            <li className='list-group-item bg-transparent p-0'>
              <a href='https://www.facebook.com/profile.php?id=100088395124275'>
                <FbIcon></FbIcon>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}
